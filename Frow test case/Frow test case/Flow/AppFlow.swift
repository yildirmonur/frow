//
//  AppFlow.swift
//  Frow test case
//
//  Created by IOS on 20.06.2021.
//

import UIKit
import RxFlow
import RxCocoa
import RxSwift
import SwiftUI

class AppFlow: Flow {
  
  var root: Presentable {
    return self.rootViewController
  }
  private let rootViewController = UINavigationController()
  
  func navigate(to step: Step) -> FlowContributors {
    guard let step = step as? AppSteps else { return .none }
    
    switch step {
      
    case .browseNewReleases:
      return navigateToNewReleases()
      
    case .tracksOfAlbum(let album):
      return navigateToTracksOfAlbum(with: album)
    }
  }
  
  private func navigateToNewReleases() -> FlowContributors {
    let viewController = NewReleasesVC(viewModel: NewReleasesViewModel(service: NewReleasesApi()))
    viewController.title = NSLocalizedString("New Albums", comment: "")
    
    self.rootViewController.pushViewController(viewController, animated: true)
    return .one(flowContributor: .contribute(withNextPresentable: viewController,
                                             withNextStepper: viewController.viewModel))
  }
  
  private func navigateToTracksOfAlbum(with album: Album) -> FlowContributors {
    let viewController = NewReleasesVC(viewModel: NewReleasesViewModel(service: NewReleasesApi()))
    viewController.title = NSLocalizedString("New Albums", comment: "")
    
    self.rootViewController.pushViewController(
      UIHostingController(
        rootView: AlbumDetail(viewModel: AlbumDetailViewModel(service: GetTracksApi(), albumId: album.id)).navigationTitle(album.name)
      ), animated: true
    )
    return .one(flowContributor: .contribute(withNextPresentable: viewController,
                                             withNextStepper: viewController.viewModel))
  }
}

class AppStepper: RxFlow.Stepper {
  let steps = PublishRelay<Step>()
  
  static let shared = AppStepper()
  
  var initialStep: Step {
    return AppSteps.browseNewReleases
  }
}
