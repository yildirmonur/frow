//
//  AppSteps.swift
//  Frow test case
//
//  Created by IOS on 12.08.2021.
//

import Foundation
import RxFlow

enum AppSteps: Step {
  
  case browseNewReleases
  case tracksOfAlbum(Album)
}

