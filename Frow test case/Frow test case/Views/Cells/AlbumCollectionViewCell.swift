//
//  AlbumCollectionViewCell.swift
//  Frow test case
//
//  Created by Lojika IOS on 12.08.2021.
//

import UIKit
import SDWebImage

class AlbumCollectionViewCell: UICollectionViewCell {
  
  private lazy var mainImage: UIImageView = {
    let image = UIImageView()
    image.contentMode = .scaleAspectFill
    image.layer.masksToBounds = true
    image.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
    return image
  }()
  
  lazy var titleLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.font = UIFont.systemFont(ofSize: 12)
    label.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
    return label
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    initUI()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    setupUI()
  }
  
  private func initUI() {
    addSubview(mainImage)
    addSubview(titleLabel)
  }
  
  private func setupUI() {
    mainImage.snp.makeConstraints { [unowned self] m in
      m.left.top.right.equalTo(self)
      m.bottom.equalTo(titleLabel.snp.top).offset(-5)
    }
    titleLabel.snp.makeConstraints { [unowned self] m in
      m.left.equalTo(mainImage.snp.left).offset(5)
      m.right.equalTo(mainImage.snp.right).offset(-5)
      m.bottom.equalTo(snp.bottom).offset(-5)
    }
  }
  
  func configure(with album: Album) {
    if !album.images.isEmpty {
      mainImage.sd_setImage(with: URL(string: album.images.first!.url), completed: .none)
    }
    
    var description = "Album name: \(album.name)"
    album.artists.forEach { artist in
      description = description + "\nartist name: \(artist.name)"
    }
    
    titleLabel.text = description
  }
}

