//
//  Track.swift
//  Frow test case
//
//  Created by Lojika IOS on 13.08.2021.
//

import Foundation

struct Tracks: Codable {
  
  let items: [Track]
}

struct Track: Codable {
  
  let id: String
  let name: String
}
