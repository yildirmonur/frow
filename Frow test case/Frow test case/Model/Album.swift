//
//  Album.swift
//  Frow test case
//
//  Created by Lojika IOS on 12.08.2021.
//

import Foundation

struct AlbumsResponse: Codable {
  
  let albums: Albums
}

struct Albums: Codable {
  
  let items: [Album]
}

struct Album: Codable {
  
  let id: String
  let album_type: String
  let name: String
  let artists: [Artist]
  let images: [AlbumImage]
}

struct Artist: Codable {
  
  let type: String
  let name: String
}

struct AlbumImage: Codable {
  
  let url: String
}
