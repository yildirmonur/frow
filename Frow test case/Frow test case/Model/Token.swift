//
//  Token.swift
//  Frow test case
//
//  Created by Lojika IOS on 12.08.2021.
//

import Foundation

struct Token: Codable {
  
  let access_token: String
  let token_type: String
  let expires_in: Int
}
