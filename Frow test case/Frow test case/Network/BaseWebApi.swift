//
//  BaseWebApi.swift
//  Frow test case
//
//  Created by IOS on 20.06.2021.
//

import Foundation
import RxCocoa
import RxSwift

// MARK: - extension for converting out RecipeModel to jsonObject

extension Encodable {
  var dictionaryValue:[String: Any?]? {
    guard let data = try? JSONEncoder().encode(self),
          let dictionary = try? JSONSerialization.jsonObject(with: data,
                                                             options: .allowFragments) as? [String: Any] else {
      return .none
    }
    return dictionary
  }
}

class BaseWebApi {
  
  enum RequestType: String {
    case post = "POST"
    case get = "GET"
    case put = "PUT"
  }
  
  enum ContentType {
    case json
    case urlEncoded
  }
  
  lazy var requestObservable = RequestObservable(config: .default)
  
  private func setHeader( request: inout URLRequest, contentType: ContentType = .json) {
    switch contentType {
    case .json:
      request.addValue("application/json;charset=UTF-8", forHTTPHeaderField:"Content-Type")
    case .urlEncoded:
      request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField:"Content-Type")
    }
    
    guard let accessToken = AccessTokenApi.shared.accessToken else { return }
    request.addValue("Bearer \(accessToken)", forHTTPHeaderField:"Authorization")
    
  }
  
  func request<T: Decodable, M: Encodable> (url: URL, type: RequestType, body: M) throws -> Observable<T?> {
    var request = URLRequest(url: url)
    request.httpMethod = type.rawValue
    do {
      let jsonBody = try JSONEncoder().encode(body)
      request.httpBody = jsonBody
    } catch { throw error }
    setHeader(request: &request)
    
    return requestObservable.callAPI(request: request)
  }
  
  func request<T: Decodable> (url: URL, type: RequestType) throws -> Observable<T>? {
    var request = URLRequest(url: url)
    request.httpMethod = type.rawValue
    setHeader(request: &request)
    
    return requestObservable.callAPI(request: request)
  }
  
  func request<T: Decodable> (url: URL, type: RequestType, body: Data?) throws -> Observable<T>? {
    var request = URLRequest(url: url)
    request.httpMethod = type.rawValue
    request.httpBody = body
    setHeader(request: &request, contentType: .urlEncoded)
    
    return requestObservable.callAPI(request: request)
  }
}
