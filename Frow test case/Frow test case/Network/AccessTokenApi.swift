//
//  AccessTokenApi.swift
//  Frow test case
//
//  Created by Lojika IOS on 12.08.2021.
//

import Foundation
import RxCocoa
import RxSwift

protocol AccessTokenApiProtocol: AnyObject {
  
  func getAccessTokenIfNeeded() throws -> Observable<Token>?
}

final class AccessTokenApi: BaseWebApi {
  
  static let shared = AccessTokenApi()
  
  private let disposeBag = DisposeBag()
  
  var accessToken: String?
  private var lastUpdatedDate: Date?
  
  func getAccessToken() -> Observable<Void> {
    return Observable.create { [unowned self] observer in
      
      if let _ = accessToken, let lastUpdatedDate = lastUpdatedDate {
        if Date().timeIntervalSince(lastUpdatedDate) < 3600 {
          observer.onNext(())
        }
      }
      
      var urlComponent = URLComponents()
      urlComponent.scheme = "https"
      urlComponent.host = "accounts.spotify.com"
      urlComponent.path = "/api/token"
      
      let body = "grant_type=\(grant_type)&client_id=\(client_id)&client_secret=\(client_secret)".data(using: .utf8)
      
      guard let url = urlComponent.url else { return Disposables.create { } }
      
      do {
        let token: Observable<Token>? = try request(url: url, type: .post, body: body)
        token?.observe(on: MainScheduler.instance).subscribe(onNext: { [unowned self] response in
          lastUpdatedDate = Date()
          accessToken = response.access_token
          observer.onNext(())
        })
        .disposed(by: disposeBag)
      } catch { observer.onError(error) }
      
      return Disposables.create { }
    }
  }
}
