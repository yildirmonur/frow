//
//  NewReleasesApi.swift
//  Frow test case
//
//  Created by Lojika IOS on 12.08.2021.
//

import Foundation
import RxCocoa
import RxSwift

protocol NewReleasesApiProtocol: AnyObject {
  
  func getNewReleases() throws -> Observable<AlbumsResponse>?
}

final class NewReleasesApi: BaseWebApi, NewReleasesApiProtocol {
  
  func getNewReleases() throws -> Observable<AlbumsResponse>? {
    let urlComponent = URLService.getBaseUrlComponents(path: "/v1/browse/new-releases")
    guard let url = urlComponent.url else { return .none }
    
    return try request(url: url, type: .get)
  }
}

final class MockNewReleasesApi: NewReleasesApiProtocol {
  
  func getNewReleases() throws -> Observable<AlbumsResponse>? {
    return .none
  }
}
