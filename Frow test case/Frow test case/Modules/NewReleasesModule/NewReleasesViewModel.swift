//
//  NewReleasesViewModel.swift
//  Frow test case
//
//  Created by Lojika IOS on 12.08.2021.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class NewReleasesViewModel: BaseViewModel {
  
  let service: NewReleasesApiProtocol
  
  let albums = BehaviorRelay<[Album]>(value: [])
  
  init(service: NewReleasesApiProtocol) {
    self.service = service
    
    super.init()
    
    getAlbums()
  }
  
  func getAlbums() {
    albums.accept([])
    isLoading.accept(true)
    AccessTokenApi.shared.getAccessToken()
      .observe(on: MainScheduler.instance)
      .subscribe(onNext: { [unowned self] in
        do {
          try service.getNewReleases()?
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [unowned self] response in
              albums.accept(response.albums.items)
            },
            onError: { error in print(error.localizedDescription) },
            onCompleted: { [weak self] in self?.isLoading.accept(false) })
            .disposed(by: disposeBag)
        }
        catch {
          print("error")
        }
      }).disposed(by: disposeBag)
  }
  
  func selectedItem(row: Int) {
    steps.accept(AppSteps.tracksOfAlbum(albums.value[row]))
  }
}
