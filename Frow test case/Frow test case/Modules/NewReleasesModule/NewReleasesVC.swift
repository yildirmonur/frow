//
//  ViewController.swift
//  Frow test case
//
//  Created by Lojika IOS on 12.08.2021.
//

import UIKit
import RxCocoa
import RxSwift
import SwiftUI

class NewReleasesVC: BaseVC {
  
  let collectionViewPadding: CGFloat = 10
  
  let viewModel: NewReleasesViewModel

  private lazy var collectionView: UICollectionView = {
    let collectionView = UICollectionView.init(frame: CGRect(x: 0,
                                                             y: 0,
                                                             width: view.frame.width,
                                                             height: view.frame.height),
                                               collectionViewLayout: collectionViewFlowLayout)
    collectionView.register(
      AlbumCollectionViewCell.self,
      forCellWithReuseIdentifier: String(describing: AlbumCollectionViewCell.self)
    )
    collectionView.backgroundColor = .white
    collectionView.delegate = self
    
    return collectionView
  }()
  
  private lazy var collectionViewFlowLayout: UICollectionViewFlowLayout = {
    let collectionViewFlowLayout = UICollectionViewFlowLayout()
    collectionViewFlowLayout.minimumLineSpacing = 10.0
    collectionViewFlowLayout.scrollDirection = .vertical
    collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: collectionViewPadding,
                                                         left: collectionViewPadding,
                                                         bottom: collectionViewPadding,
                                                         right: collectionViewPadding)
    
    return collectionViewFlowLayout
  }()
  
  init(viewModel: NewReleasesViewModel) {
    self.viewModel = viewModel
    
    super.init(nibName: .none, bundle: .none, baseViewModel: viewModel)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    initUI()
    bindUI()
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    setupUI()
  }
  
  private func initUI() {
    view.addSubview(collectionView)
  }
  
  private func setupUI() {
    collectionView.snp.makeConstraints { [unowned self] (m) in
      m.top.equalTo(view.safeAreaLayoutGuide.snp.top)
      m.left.right.bottom.equalTo(view.safeAreaLayoutGuide)
    }
  }
  
  private func bindUI() {
    viewModel
      .albums
      .asObservable()
      .observe(on: MainScheduler.instance)
      .bind(to: collectionView.rx.items(cellIdentifier: String(describing: AlbumCollectionViewCell.self)))
      { [weak self] (index, property, cell) in
        if let cell = cell as? AlbumCollectionViewCell, let album = self?.viewModel.albums.value[index] {
          cell.configure(with: album)
        }
      }
      .disposed(by: disposeBag)
  }
}

extension NewReleasesVC: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    let width = collectionView.bounds.width
    let cellWidth = (width - collectionViewPadding * 3) / 2
    return CGSize(width: cellWidth, height: cellWidth + 20)
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    viewModel.selectedItem(row: indexPath.row)
  }
}
