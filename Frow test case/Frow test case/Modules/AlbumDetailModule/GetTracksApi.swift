//
//  GetTracksApi.swift
//  Frow test case
//
//  Created by Lojika IOS on 13.08.2021.
//

import Foundation
import RxCocoa
import RxSwift

protocol GetTracksApiProtocol: AnyObject {
  
  func getTracks(with albumId: String) throws -> Observable<Tracks>?
}

final class GetTracksApi: BaseWebApi, GetTracksApiProtocol {
  
  func getTracks(with albumId: String) throws -> Observable<Tracks>? {
    let urlComponent = URLService.getBaseUrlComponents(path: "/v1/albums/\(albumId)/tracks")
    guard let url = urlComponent.url else { return .none }
    
    return try request(url: url, type: .get)
  }
}

final class MockGetTracksApi: GetTracksApiProtocol {
  
  func getTracks(with albumId: String) throws -> Observable<Tracks>? {
    return .none
  }
}

