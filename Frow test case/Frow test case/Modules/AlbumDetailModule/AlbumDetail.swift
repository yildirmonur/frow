//
//  AlbumDetail.swift
//  Frow test case
//
//  Created by Lojika IOS on 13.08.2021.
//

import SwiftUI

struct AlbumDetail: View {
  
  @StateObject var viewModel: AlbumDetailViewModel
  
  init(viewModel: AlbumDetailViewModel) {
    _viewModel = StateObject(wrappedValue: viewModel)
  }
  
  var body: some View {
    List {
      ForEach(viewModel.tracks, id: \.id) { track in
        Text(track.name).padding()
      }
    }
    .onAppear(perform: { viewModel.getTracks() })
  }
}
