//
//  AlbumDetailViewModel.swift
//  Frow test case
//
//  Created by Lojika IOS on 13.08.2021.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class AlbumDetailViewModel: BaseViewModel, ObservableObject {
  
  let service: GetTracksApiProtocol
  private let albumId: String
  @Published private(set) var tracks = [Track]()
  
  init(service: GetTracksApiProtocol, albumId: String) {
    self.service = service
    self.albumId = albumId
    
    super.init()
  }
  
  func getTracks() {
    tracks = []
    isLoading.accept(true)
    AccessTokenApi.shared.getAccessToken()
      .observe(on: MainScheduler.instance)
      .subscribe(onNext: { [unowned self] in
        do {
          try service.getTracks(with: albumId)?
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [unowned self] response in
              tracks = response.items
            },
            onError: { error in print(error.localizedDescription) },
            onCompleted: { [weak self] in self?.isLoading.accept(false) })
            .disposed(by: disposeBag)
        }
        catch {
          print("error")
        }
      }).disposed(by: disposeBag)
  }
}
