//
//  BaseViewController.swift
//  thecats
//
//  Created by IOS on 20.06.2021.
//

import UIKit
import RxSwift

class BaseVC: UIViewController {
  
  let disposeBag = DisposeBag()
  
  private var loadingView: UIView? {
    didSet {
      oldValue?.removeFromSuperview()
    }
  }
  private let baseViewModel: BaseViewModel
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
  }
  
  init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, baseViewModel: BaseViewModel) {
    self.baseViewModel = baseViewModel
    super.init(nibName: .none, bundle: .none)
    
    initUI()
    subscribe()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func initUI() {
    view.backgroundColor = .white
  }
  
  private func subscribe() {
    baseViewModel.isLoading.asObservable().subscribe { [unowned self] isLoading in
      isLoading ? showLoadingView() : loadingView?.removeFromSuperview()
    }.disposed(by: disposeBag)
  }
  
  private func showLoadingView() {
    loadingView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
    loadingView?.backgroundColor = UIColor(hex: "#000000A0")
    
    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: UIScreen.main.bounds.width / 2 - 25,
                                                                 y: UIScreen.main.bounds.height / 2 - 25,
                                                                 width: 50,
                                                                 height: 50))
    loadingIndicator.hidesWhenStopped = true
    loadingIndicator.style = .medium
    loadingIndicator.startAnimating()
    
    loadingView?.addSubview(loadingIndicator)
    
    if let window = UIApplication.shared.windows.first, let loadingView = loadingView {
      window.addSubview(loadingView)
    }
  }
}

// MARK: - UI objects

extension BaseVC {
  
  func createActivityIndicator() -> UIActivityIndicatorView{
    let spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
    spinner.startAnimating()
    spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.view.bounds.width, height: CGFloat(44))
    
    return spinner
  }
}
