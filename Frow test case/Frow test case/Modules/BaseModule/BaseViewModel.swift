//
//  BaseViewModel.swift
//  thecats
//
//  Created by IOS on 20.06.2021.
//

import RxFlow
import RxSwift
import RxCocoa

class BaseViewModel: RxFlow.Stepper {
  
  let disposeBag = DisposeBag()
  
  let isLoading: BehaviorRelay<Bool> = .init(value: false)
  let steps = PublishRelay<Step>()
}

